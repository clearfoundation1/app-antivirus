<?php

$lang['antivirus_app_description'] = 'Додаток «Антивірус шлюзу» використовує центральний антивірусний механізм для сканування Інтернету, FTP, пошти тощо. Він захищає пристрої, підключені до вашої мережі, зупиняючи зловмисне програмне забезпечення, перш ніж воно встигне досягти ваших користувачів.';
$lang['antivirus_app_name'] = 'Антивірус шлюзу';
$lang['antivirus_app_tooltip'] = 'Зашифровані файли часто використовуються для доставки зловмисного програмного забезпечення, тому будьте обережні, дозволяючи такі типи файлів через антивірусну систему.';
$lang['antivirus_block_encrypted_archive_flag_invalid'] = 'Недійсне значення для зашифрованого архіву';
$lang['antivirus_block_encrypted_files'] = 'Блокувати зашифровані файли';
$lang['antivirus_every_two_hours'] = 'Кожні дві години';
$lang['antivirus_maximum_file_size'] = 'Макс. розмір файлу в zip';
$lang['antivirus_maximum_file_size_invalid'] = 'Maximum file size is invalid.';
$lang['antivirus_maximum_files'] = 'Макс. кількість файлів у zip';
$lang['antivirus_maximum_files_invalid'] = 'Maximum number of files is invalid.';
$lang['antivirus_maximum_recursion'] = 'Макс. рекурсія в zip';
$lang['antivirus_maximum_recursion_invalid'] = 'Максимальна рекурсія недійсна.';
$lang['antivirus_phishing_always_block_cloak_invalid'] = 'SSL cloaking scan state is invalid.';
$lang['antivirus_phishing_always_block_ssl_mismatch_invalid'] = 'SSL URL mismatch scan state is invalid.';
$lang['antivirus_phishing_scan_urls_state_invalid'] = 'Scan URLs state is invalid.';
$lang['antivirus_phishing_signatures_state_invalid'] = 'Phishing signatures scan state is invalid.';
$lang['antivirus_twice_a_day'] = 'Два рази на день';
$lang['antivirus_update_interval'] = 'Інтервал оновлення';
$lang['antivirus_updates_per_day_invalid'] = 'Кількість оновлень за день недійсна.';
